export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:/usr/local/bin

export NODE_PATH=/usr/local/lib/node_modules:$NODE_PATH

export HISTCONTROL=erasedups
export HISTSIZE=1000
shopt -s histappend

export LESS="-R"
export EDITOR="vim"

source ~/.bashrc