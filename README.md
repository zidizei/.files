# .files

These are the .files on my current machines.

It's organized by [Dotify](https://github.com/mattdbridges/dotify), so just do a quick `dotify github Zidizei/.files` on your [dotified machine](https://github.com/mattdbridges/dotify#installation) to pretend you're me.
