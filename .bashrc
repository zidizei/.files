PLATFORM=''
_uname=$(uname)
if [[ "$_uname" == 'Linux' ]]
then
	PLATFORM='linux'

elif [[ "$_uname" == 'Darwin' ]]
then
	PLATFORM='mac'
fi



if [[ "$PLATFORM" == 'mac' ]]
then
	PS1='\[\e[0;37m\]\h:\[\e[0;34m\]\W \[\e[0;37m\]\u\$\[\e[m\] '
	
elif [[ "$PLATFORM" == 'linux' ]]
then
	PS1='\[\e[0;33m\]\h:\[\e[m\]\[\e[1;34m\]\W \u\[\e[m\]\[\e[1;33m\]\$\[\e[m\] \[\e[m\]'

	function vhosts
	{
		Master_Vhosts_Path='/etc/httpd/conf/extra/httpd-vhosts.conf'
		Sub_Vhosts_Path='/etc/httpd/conf/vhosts/'


		I=0

		IN="$@"
		OIFS=$IFS
		IFS=' '
		arr2=$IN
		for x in $arr2
		do
		   	arr1[I]="$x"
		   	I=$((I+1))
		done

		arr1[I]=''


		if [[ "${arr1[0]}" == "" ]]
		then
			sudo vim ${Master_Vhosts_Path}
		else
			sudo vim ${Sub_Vhosts_Path}${arr1[0]}
		fi
	}
fi

source ~/.bashrc_aliases